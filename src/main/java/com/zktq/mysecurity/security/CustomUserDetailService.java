package com.zktq.mysecurity.security;

import com.zktq.mysecurity.entity.CustomUser;
import com.zktq.mysecurity.service.CustomUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    CustomUserService customUserService;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if ("".equals(username) || username == null) {
            System.out.println("username is empty: " + username);
            return null;
        }
        CustomUser customUser = customUserService.getUserByName(username);
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(customUser.getAuthority()));
        return new User(customUser.getUsername(), customUser.getPassword(), authorities);
    }
}
