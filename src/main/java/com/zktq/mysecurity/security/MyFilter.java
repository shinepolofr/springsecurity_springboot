package com.zktq.mysecurity.security;

import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MyFilter extends AbstractAuthenticationProcessingFilter {
    protected MyFilter() {
        super("/login");
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {
        if (!httpServletRequest.getMethod().equals("POST")) {
            throw new AuthenticationServiceException("只支持POST请求");
        }
        String name = httpServletRequest.getParameter("usernames");
        MyAuthentication authRequest = new MyAuthentication(name);
        authRequest.setDetails("其他参数");
        return getAuthenticationManager().authenticate(authRequest);
    }
}
