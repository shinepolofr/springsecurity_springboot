package com.zktq.mysecurity.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.security.access.vote.RoleVoter;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.expression.WebExpressionVoter;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Bean
    protected UserDetailsService userDetailsService(){
        return new CustomUserDetailService();
    }

    @Bean
    protected MyAuthenticationProvider myAuthenticationProvider(){
        return new MyAuthenticationProvider();
    }


    protected AccessDecisionManager accessDecisionManager(){
        List<AccessDecisionVoter<? extends Object>> accessDecisionVoterList = Arrays.asList(
                new RoleVoter(),
                new WebExpressionVoter(),
                new MyAccessDecisionVoter()
        );
        return new AffirmativeBased(accessDecisionVoterList);
    }

    @Bean
    public AuthenticationProvider authenticationProvider2(){
        return new AuthenticationProvider() {
            @Override
            public Authentication authenticate(Authentication authentication) throws AuthenticationException {
                String username = authentication.getPrincipal().toString();
                if (!"xuer".equals(username)) {
                    throw new BadCredentialsException("用户名错误");
                }
                List<GrantedAuthority> authorities = new ArrayList<>();
                authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
                return new MyAuthentication(username,authorities);
            }

            @Override
            public boolean supports(Class<?> authentication) {
                return MyAuthentication.class.isAssignableFrom(authentication);
            }
        };
    }

    @Bean
    public MyFilter myFilter() throws Exception {
        MyFilter myFilter = new MyFilter();
        myFilter.setAuthenticationManager(authenticationManager());
        myFilter.setFilterProcessesUrl("/login");
        myFilter.setAuthenticationFailureHandler(new SimpleUrlAuthenticationFailureHandler("/login?error"));
        myFilter.setAuthenticationSuccessHandler(new SimpleUrlAuthenticationSuccessHandler("/home"));
        return myFilter;
    }
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .authenticationProvider(myAuthenticationProvider())
                //.authenticationProvider(authenticationProvider2())
                .userDetailsService(userDetailsService()).passwordEncoder(new PasswordEncoder() {
            @Override
            public String encode(CharSequence rawPassword) {
                return rawPassword.toString();
            }

            @Override
            public boolean matches(CharSequence rawPassword, String encodedPassword) {
                if (rawPassword.toString().equals(encodedPassword)) {
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests().antMatchers("/").permitAll()
                .antMatchers("/login").permitAll()
                .anyRequest().authenticated().accessDecisionManager(accessDecisionManager())
                .antMatchers("/others").hasRole("NOUSER")
                .and()
                .formLogin()
                    .usernameParameter("usernames").passwordParameter("password")
                    .loginPage("/login").defaultSuccessUrl("/home")
                .and()
                .logout().logoutSuccessUrl("/")
                .and()
                //.addFilterAt(myFilter(), UsernamePasswordAuthenticationFilter.class)
                .csrf().disable()
                .rememberMe().rememberMeParameter("rememberme");

    }

    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
