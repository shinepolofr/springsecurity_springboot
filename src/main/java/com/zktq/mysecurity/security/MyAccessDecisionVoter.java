package com.zktq.mysecurity.security;

import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.core.Authentication;

import java.util.Collection;

public class MyAccessDecisionVoter implements AccessDecisionVoter {
    @Override
    public boolean supports(ConfigAttribute attribute) {
        return true;
    }

    @Override
    public int vote(Authentication authentication, Object object, Collection collection) {
        int result = 0;
        String name = authentication.getName();
        if (name != null && name.contains("xuer")) {
            result = 1;
        } else {
            result = -1;
        }
        return result;
    }

    @Override
    public boolean supports(Class clazz) {
        return true;
    }
}
