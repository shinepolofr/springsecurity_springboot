package com.zktq.mysecurity.controller;

import com.zktq.mysecurity.service.CustomUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;


@Controller
public class LoginController implements WebMvcConfigurer {

    @Autowired
    CustomUserService customUserService;

    @RequestMapping("/home")
    public String index(Model model){
        List<String> list = new ArrayList<>();
        list.add("test title");
        list.add("test content");
        list.add("welcom admin");
        model.addAttribute("list", list);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        System.out.println("Princeple "+authentication.getPrincipal().getClass().toString());
//        System.out.println(authentication.getAuthorities().toString());
//        System.out.println(authentication.getCredentials().toString());
//        System.out.println(authentication.getDetails().toString());
//        System.out.println(authentication.getName().toString());
        return "home";
    }

    @PreAuthorize(" hasAnyRole('ROLE_NOUSER','ROLE_ADMIN')")
    @RequestMapping("/others")
    public String toHello() {
        return "other/hello";
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping("/users")
    public String touser() {
        return "other/hello";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping("/admin")
    @ResponseBody
    public String toAdamin() {
        return "hello admin";
    }

    @RequestMapping("/insert_user")
    @ResponseBody
    public String insertUser(String username, String password, String authorities) {
        try {
            customUserService.insertCustomUser(username, password, authorities);
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
        return "success insert";
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        System.out.println("success");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/").setViewName("login");
    }
}
