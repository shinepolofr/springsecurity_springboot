package com.zktq.mysecurity.entity;

public class CustomUser {
    private String id;
    private String username;
    private String password;
    private String authorities;

    public CustomUser() {
    }

    public CustomUser(String id, String username, String password, String authorities) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.authorities = authorities;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuthority() {
        return authorities;
    }

    public void setAuthority(String authority) {
        this.authorities = authority;
    }
}
