package com.zktq.mysecurity.dao;

import com.zktq.mysecurity.entity.CustomUser;

public interface CustomUserDao {
    public CustomUser selectUserByName(String username) throws Exception;

    public void insertCustomUser(CustomUser customUser);
}
