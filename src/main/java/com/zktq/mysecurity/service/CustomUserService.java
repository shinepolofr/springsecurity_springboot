package com.zktq.mysecurity.service;

import com.zktq.mysecurity.dao.CustomUserDao;
import com.zktq.mysecurity.entity.CustomUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.UUID;

@Service
public class CustomUserService {
    @Autowired
    private CustomUserDao customUserDao;

    public CustomUser getUserByName(String username) {
        CustomUser customUser = new CustomUser();
        try {
            customUser = customUserDao.selectUserByName(username);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        System.out.println("test"+customUser.getAuthority());
        return customUser;
    }

    public void insertCustomUser(String username, String password, String authorities)throws Exception {
        CustomUser customUser = new CustomUser();
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        customUser.setId(UUID.randomUUID().toString());
        customUser.setUsername(username);
        customUser.setPassword(bCryptPasswordEncoder.encode(password));
        customUser.setAuthority(authorities);
        customUserDao.insertCustomUser(customUser);
    }
}
